
/**
 * @author Riccardo Mioli, 825373, riccardo.mioli2@studio.unibo.it
 * @version 0.0.7rc
 *
 * E' necessario trasportare un certo numero di litri di gas nel minor numero
 * possibile di cisterne. Il problema viene affrontato tramite programmazione
 * dinamica ed e' impostato nello stesso modo del problema del resto visto a
 * lezione. Il resto da erogare sono i litri di gas e le monete sono le
 * cisterne. La complessita' della costruzione della matrice risolutiva e' O(nm)
 * ossia il numero delle righe per il numero delle colonne.
 *
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Arrays;

public final class Esercizio4 {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        //Settaggio scanner.
        Scanner inputStream = null;
        try {
            inputStream = new Scanner(new File(args[0]));
        } catch (FileNotFoundException e) {
            System.out.println("Sembra che il file di input non sia stato trovato");
            System.exit(0);
        }
        int litriGas = inputStream.nextInt();
        System.out.println("Devo trasportare questo gas: " + litriGas);
        int[] tipiCisterna = new int[inputStream.nextInt()];
        int index = 0;
        //Il file ha l'ultima riga vuota, la evito con la seconda condizione del while.
        while (inputStream.hasNextLine() && inputStream.hasNext()) {
            tipiCisterna[index] = inputStream.nextInt();
            index++;
        }
        System.out.println("Con le seguenti cisterne: " + Arrays.toString(tipiCisterna));
        int[][] tabella = costruisciTabella(tipiCisterna, litriGas);
        String toWrite = costruisciStringa(tabella, tipiCisterna, litriGas, args[1]);
        writeOnFile(args[1], toWrite);
    }

    /**
     * Metodo per la costruzione della tabella tramite programmazione dinamica.
     *
     * @param tipiCisterna Un array contenente i tipi della cisterna
     * @param litriGas Il totale dei litri di gas da trasportare
     * @return La tabella risolutiva
     */
    private static int[][] costruisciTabella(int[] tipiCisterna, int litriGas) {
        /*
         * La tabella ha un numero di righe pari al numero delle cisterne e un
         * numero di colonne pari ai litri di gas.
         */
        int[][] tabella = new int[tipiCisterna.length][litriGas + 1];//
        //Quando ho 0 litri di gas da trasportare non uso nessuna cisterna.
        for (int index = 0; index < tipiCisterna.length; index++) {
            tabella[index][0] = 0;
        }
        /*
        La prima riga e' popolata nel seguente modo: se il gas da trasportare
        e' un multiplo della cisterna allora prendo un numero di cisterne
        pari a nLitri/capienzaCisterna.
         */
        for (int index = 1; index < litriGas + 1; index++) {
            if (index % tipiCisterna[0] == 0) {
                tabella[0][index] = index / tipiCisterna[0];
            } else {
                /*
                Altrimenti servono infinite cisterne. Il -1 serve per evitare
                possibili overflow
                 */
                tabella[0][index] = Integer.MAX_VALUE - 1;
            }
        }
        /*
        Dalla seconda colonna in poi e dalla seconda riga parto a popolare la tabella
         */
        for (int cisterna = 1; cisterna < tipiCisterna.length; cisterna++) {
            for (int litroEsaminato = 1; litroEsaminato < litriGas + 1; litroEsaminato++) {
                //Se questa cisterna viene riempita completamente ne prendo una
                if (tipiCisterna[cisterna] == litroEsaminato) {
                    tabella[cisterna][litroEsaminato] = 1;
                } else if (tipiCisterna[cisterna] > litroEsaminato) {
                    //se la cisterna e' piu' grande prendo la cisterna che avevo scelto nella riga sopra
                    tabella[cisterna][litroEsaminato] = tabella[cisterna - 1][litroEsaminato];
                } else {
                    /*
                    Ultimo caso (se la cisterna e' piu' piccola): prendo il numero minimo di cisterne fra quelle che 
                    avevo preso sopra e quelle piu' a sinistra (lo spostamento a sx e' pari alla capienza della cisterna
                    in questo modo ottengo il numero di cisterne che avrei senza questa cisterna).
                     */
                    tabella[cisterna][litroEsaminato] = Math.min(tabella[cisterna - 1][litroEsaminato], tabella[cisterna][litroEsaminato - tipiCisterna[cisterna]] + 1);
                }
            }
        }
        return tabella;
    }

    /**
     * Metodo per la costruzione della stringa risolutiva. La tabella viene
     * risalita dal fondo per trovare le cisterne che ho scelto durante la
     * costruzione della tabella.
     *
     * @param tabella La tabrlla risolutiva
     * @param tipiCisterna I tipi di cisterna
     * @param litriGas I litri totali che devo trasportare
     * @param outputDestination Il path del file di output
     * @return
     */
    private static String costruisciStringa(int[][] tabella, int[] tipiCisterna, int litriGas, String outputDestination) {
        StringBuilder toPrint = new StringBuilder();
        //Se l'ultima cella contiene il numero max di cisterne allora non c'e' soluzione
        if (tabella[tipiCisterna.length - 1][litriGas] >= Integer.MAX_VALUE - 1) {
            writeOnFile(outputDestination, "-1");
        } else {
            int cisterna = tipiCisterna.length - 1;
            int litroEsaminato = litriGas;
            toPrint.append(tabella[cisterna][litroEsaminato] + " ");
            while (litroEsaminato > 0) {
                if (cisterna > 0 && tabella[cisterna][litroEsaminato] == tabella[cisterna - 1][litroEsaminato]) {
                    /*
                    Se il numero di cisterne e' uguale a quello della riga sopra
                    allora vuol dire che viene da su quindi salgo alla riga superiore.
                     */
                    cisterna--;
                } else {
                    /*
                    Se non e' uguale allora vuol dire che viene da sx quindi aggiungo questa
                    cisterna all'elenco delle scelte e mi sposto a sinista.
                     */
                    System.out.println("Ho scelto la cisterna da " + tipiCisterna[cisterna]);
                    toPrint.replace(2, 2, "\n" + tipiCisterna[cisterna]);
                    litroEsaminato -= tipiCisterna[cisterna];
                }
            }
        }
        return toPrint.toString();
    }

    /**
     * Metodo per scrivere su file
     *
     * @param outputDestination Richiede la destinazione su cui scrivere
     * @param toWrite Richiede cosa scrivere
     */
    private static void writeOnFile(String outputDestination, String toWrite) {
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(outputDestination);
        } catch (FileNotFoundException e) {
            System.out.println("Non sono riuscito a creare il file di output nella seguente posizione: " + outputDestination);
        }
        outputStream.println(toWrite);
        outputStream.close();
        System.exit(0);
    }
}
