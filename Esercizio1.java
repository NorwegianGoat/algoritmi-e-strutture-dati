
/**
 * @author Riccardo Mioli, 825373, riccardo.mioli2@studio.unibo.it
 * @version 0.0.5rc
 *
 * E' data in input una stringa di caratteri. Si vuole controllare che alcuni
 * caratteri di apertura (dati in input) vengano chiusi con i corrispondenti
 * caratteri di chiusura. Il costo della operazione e' O(n) perche' e'
 * necessario scorrere tutta la stringa Si vuole anche controllare il massimo
 * livello di profondita' raggiunto dagli open char. Per risolvere il problema
 * viene utilizzato uno stack. I caratteri di apertura sono inseriti nello
 * stack. Quando si trova un caratteri di chiusura se ne controlla l'indice
 * nell'array dei caratteri di chiusura (costo O(n) e si confronta con l'indice
 * del carattere di apertura contenuto nello stack (costo O(n)). Se gli indici
 * coincidono si puo' procedere a rimuovere l'open char dallo stack.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;
import java.util.Stack;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.ArrayList;

public final class Esercizio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        //Settaggio scanner.
        Scanner inputStream = null;
        try {
            inputStream = new Scanner(new File(args[0]));
        } catch (FileNotFoundException e) {
            System.out.println("Sembra che il file di input non sia stato trovato");
            System.exit(0);
        }

        //Lettura dati input
        int nTipiParentesi = inputStream.nextInt();
        inputStream.nextLine();
        ArrayList caratteriApertura = new ArrayList(nTipiParentesi);
        ArrayList caratteriChiusura = new ArrayList(nTipiParentesi);
        String tipiParentesi = inputStream.nextLine();
        /*Per popolare i due array in una volta uso questo offset. L'offset
        consente di spostarsi della meta' dei caratteri+1 (per saltare lo spazio).
         */
        int offset = nTipiParentesi + 1;
        for (int index = 0; index < nTipiParentesi; index++) {
            //In questo modo popolo i due array con un solo ciclo for.
            caratteriApertura.add(tipiParentesi.charAt(index));
            caratteriChiusura.add(tipiParentesi.charAt(offset + index));
        }
        System.out.println("Caratteri apertura: " + Arrays.toString(caratteriApertura.toArray()));
        System.out.println("Caratteri chiusura: " + Arrays.toString(caratteriApertura.toArray()));
        String referenceString = inputStream.nextLine();
        System.out.println("Stringa esaminata: " + referenceString);

        Stack tempesta = new Stack();
        Integer profondita = 0;
        int semaforoIncremento = 0;
        //Per ogni carattere della referenceString controllo se e' un carattere di apertura o di chiusura.
        for (int index = 0; index < referenceString.length(); index++) {
            //Se e' un carattere di apertura
            if (caratteriApertura.contains(referenceString.charAt(index))) {
                //lo aggiungo allo stack contenente le parentesi che si sono aperte
                System.out.println("Il carattere di apertura: " + referenceString.charAt(index) + " viene aggiunto allo stack");
                tempesta.push(referenceString.charAt(index));
                if (semaforoIncremento == 0) {
                    //e aggiungo 1 al calcolo della profondita'
                    profondita++;
                    System.out.println("La nuova profondita' e': " + profondita);
                } else {
                    semaforoIncremento--;
                    System.out.println("Ci sono stati dei caratteri di chiusura, la profondita' verra' reincrementata fra : " + semaforoIncremento + " caratteri di apertura");
                }
                //Se e' un carattere di chiusura il corrispondente carattere di apertura nella mia pila va rimosso.
            } else if (caratteriChiusura.contains(referenceString.charAt(index))) {
                //TempestaPop si occupa di controllare di poter effettuare la rimozione
                tempesta = tempestaPop(caratteriApertura, caratteriChiusura, referenceString.charAt(index), tempesta, args[1]);
                //Qualora la parentesi sia di chiusura non devo contare la profondita'
                semaforoIncremento++;
            }
        }
        /*Se esco dal for e la pila e' vuota significa che tutte le parentesi sono state chiuse, 
        altrimenti una delle parentesi e' rimasta aperta e questo non va bene.
         */
        if (tempesta.empty()) {
            System.out.println("La profondita' e': " + profondita.toString());
            writeOnFile(args[1], profondita.toString());
        } else {
            System.out.println("La profondita' e': -1 ");
            writeOnFile(args[1], "-1");
        }
    }

    /**
     * Metodo per rimuovere dalla pila un carattere di apertura
     *
     * @param caratteriApertura E' l'array contenente i caratteri di apertura
     * @param caratteriChiusura E' l'array contenente i caratteri di chiusura
     * @param carattereEsaminato E' il carattere che devo controllare
     * @param tempesta E' lo stack di caratteri
     * @param outputDestination La stringa contenente il path del file di output
     * @return Ritorna la pila aggiornata
     */
    private static Stack tempestaPop(ArrayList caratteriApertura, ArrayList caratteriChiusura, char carattereEsaminato, Stack tempesta, String outputDestination) {
        try {
            //Se l'indice del carattere di apertura e di chiusura coincidono allora
            if (caratteriChiusura.indexOf(carattereEsaminato) == caratteriApertura.indexOf(tempesta.peek())) {
                //posso rimuovere il carattere di apertura in cima allo stack.
                System.out.println("Il carattere di chiusura: " + carattereEsaminato + " corrisponde al carattere di apertura: " + tempesta.peek() + " lo rimuovo");
                tempesta.pop();
            } else {
                /*Se i due indici non coincidono significa che il carattere di 
                apertura e' stato chiuso con un catattere di chiusura non corrispondente.
                e.g. (([))) */
                System.out.println("Il carattere di chiusura: " + carattereEsaminato + " non corrisponde al carattere di apertura: " + tempesta.peek());
                writeOnFile(outputDestination, "-1");
            }
        } catch (EmptyStackException e) {
            /*Se finisco in questo catch significa che la stringa termina con un carattere di chiusura "non bilanciato".
            Se fosse bilanciato significherebbe che nella pila ci dovrebbe essere almeno un ultima parentesi aperta
            e.g.) (([]))] L'ultima chiusura e' di troppo! */
            System.out.println("Il carattere di chiusura: " + caratteriChiusura.indexOf(carattereEsaminato) + " non e' bilanciato");
            writeOnFile(outputDestination, "-1");
        }
        return tempesta;
    }

    /**
     * Metodo per scrivere su file
     *
     * @param outputDestination Richiede la destinazione su cui scrivere
     * @param toWrite Richiede cosa scrivere
     */
    private static void writeOnFile(String outputDestination, String toWrite) {
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(outputDestination);
        } catch (FileNotFoundException e) {
            System.out.println("Non sono riuscito a creare il file di output nella seguente posizione: " + outputDestination);
        }
        outputStream.println(toWrite);
        outputStream.close();
        System.exit(0);
    }
}
