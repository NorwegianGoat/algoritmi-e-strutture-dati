
/**
 * @author Riccardo Mioli, 825373, riccardo.mioli2@studio.unibo.it
 * @version 0.1.4rc
 *
 * E' dato in input un albero binario, viene richiesto di verificare che questo
 * albero sia un BST. La stringa di input rappresenta una previsita (root,
 * sottalbero sx, sottoalbero dx). Per risolvere il problema viene costruito un
 * albero. Per la costruzione viene usato uno stack per effettuare comodamente
 * le operazioni di link dei nodi. La costruzione dell'albero ha un costo O(n),
 * dopodiche' si verifica ricorsivamente che ogni nodo rispetti le
 * caratteristiche di un BST (si rimanda alla doc del metodo per una descrizione
 * piu' accurata. I nodi vengono visitati tutti per un costo totale di O(n).
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;
import java.util.Stack;
import java.io.PrintWriter;
import java.util.InputMismatchException;

public final class Esercizio2 {

    /**
     * La classe nodo rappresenta un nodo. Ogni nodo ha un padre, un figlio dx,
     * un figlio sx e un valore
     */
    private static final class Node {

        private Node parent;
        private Node left;
        private Node right;
        private Integer value;

        public Node(Node parent, Integer value) {
            this.parent = parent;
            this.left = null;
            this.right = null;
            this.value = value;
        }

        public void setLeft(Node leftSon) {
            this.left = leftSon;
        }

        public void setRight(Node rightSon) {
            this.right = rightSon;
        }

        public Node getParent() {
            return parent;
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }

        public Integer getValue() {
            return value;
        }

    }

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        //Viene ritornata la root dell'albero.
        Node root = bstCreator(args[0]);
        /*
        La root viene agganciata a due speciali nodi min e max che servono per
        lanciare la procedura ricorsiva per la ricerca del minimo e del massimo
        La root sta rispettivamente a sx del nodo massimo e a dx del nodo minimo
         */
        Node max = new Node(null, Integer.MAX_VALUE);
        max.setLeft(root);
        Node min = new Node(null, Integer.MIN_VALUE);
        min.setRight(root);
        //Il metodo ricorsivo scansiona l'albero.
        if (checkBST(root, min, max)) {
            System.out.println("E' un bst");
            writeOnFile(args[1], "true");
        } else {
            System.out.println("NON e' un bst");
            writeOnFile(args[1], "false");
        }
    }

    /**
     * Metodo per creare l'albero
     *
     * @param source La stringa contenente il path per la lettura del file
     * @return La root dell'albero
     */
    private static Node bstCreator(String source) {
        Scanner inputStream = null;
        try {
            inputStream = new Scanner(new File(source));
        } catch (FileNotFoundException e) {
            System.out.println("Sembra che il file di input non sia stato trovato");
            System.exit(0);
        }

        String separatore;
        Stack<Node> ortoBotanico = new Stack();
        Integer numeroEsaminato = 0;
        //Consumo la prima parentesi tonda.
        inputStream.next();
        //Setto la root.
        Node root = new Node(null, inputStream.nextInt());
        ortoBotanico.push(root);
        while (inputStream.hasNext()) {
            separatore = inputStream.next();
            //Se ho il carattere di apertura il prossimo numero e' un figlio sx
            if (separatore.equals("(")) {
                try {
                    //Questo try si rende necessario per trattare il caso in cui si abbia "-"
                    numeroEsaminato = inputStream.nextInt();
                } catch (InputMismatchException e) {
                    numeroEsaminato = null;
                }
                System.out.println("Sto inserendo come figlio SX di: " + ortoBotanico.peek().getValue() + " " + numeroEsaminato);
                Node leftSon = new Node(ortoBotanico.peek(), numeroEsaminato);
                //Una volta creato il nodo lo attacco come figlio sx al primo nodo dello stack
                ortoBotanico.peek().setLeft(leftSon);
                //Inserisco nello stack il figlio sx
                ortoBotanico.push(leftSon);
            } else if (separatore.equals(",")) {
                //Se ho una virgola il prossimo valore sara' un figlio dx
                try {
                    numeroEsaminato = inputStream.nextInt();
                } catch (InputMismatchException e) {
                    numeroEsaminato = null;
                }
                //Rimuovo dallo stack il figlio dx
                Node leftSon = ortoBotanico.pop();
                //Rimuovo il padre
                Node parent = ortoBotanico.pop();
                System.out.println("Sto inserendo come figlio DX di: " + parent.getValue() + " " + numeroEsaminato);
                Node rightSon = new Node(parent, numeroEsaminato);
                parent.setRight(rightSon);
                ortoBotanico.push(rightSon);
                //Inserisco il figlio dx nello stack
            } else if (separatore.equals(")")) {
                //E' un carattere di chiusura, reinserisco il padre del figlio dx nello stack.
                ortoBotanico.push(ortoBotanico.pop().getParent());
            }
        }
        return root;
    }

    /**
     * Questo metodo ricorsivo e' fondamentale per controllare la correttezza
     * dell'albero binario. I figli sx devono essere compresi fra il
     * predecessore e il successore (che e' loro padre). I figli dx devono
     * essere compresi fra il predecessore (loro padre) e il successore. In caso
     * i nodi siano ai lati esterni dell'albero il pred e in succ sono -inf e
     * +inf
     *
     * @param root Il nodo da controllare
     * @param lowerBound Il limite inferiore
     * @param upperBound Il limite superiore
     * @return Un booleano che consente di sapere se l'albero binario inserito
     * e' un BST
     */
    private static boolean checkBST(Node root, Node lowerBound, Node upperBound) {
        /*Questo caso e' per le foglie, significa che sono arrivato in fondo senza che la 
        condizione di upper e lower bound sia stata violata
         */
        if (root == null) {
            return true;
        }
        //Se il valore e' null (-) passo oltre e controllo i figli dx e sx con gli upper e lower bound precedenti
        if (root.getValue() == null) {
            return checkBST(root.getLeft(), lowerBound, upperBound) && checkBST(root.getRight(), lowerBound, upperBound);
        }
        System.out.println("Il nodo che sto valutando vale: " + root.getValue() + " Lower Bound: " + lowerBound.getValue() + " Upper Bound: " + upperBound.getValue());
        //Se il valore non e' compeso fra il lower bound e l'upper allora non e' un BST
        if (root.getValue() < lowerBound.getValue() || root.getValue() > upperBound.getValue()) {
            return false;
        }
        //Se non sono entrato in nessuno dei casi precedenti procedo verso il basso
        return checkBST(root.getLeft(), lowerBound, root) && checkBST(root.getRight(), root, upperBound);
    }

    /**
     * Metodo per scrivere su file
     *
     * @param outputDestination Richiede la destinazione su cui scrivere
     * @param toWrite Richiede cosa scrivere
     */
    private static void writeOnFile(String outputDestination, String toWrite) {
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(outputDestination);
        } catch (FileNotFoundException e) {
            System.out.println("Non sono riuscito a creare il file di output nella seguente posizione: " + outputDestination);
        }
        outputStream.println(toWrite);
        outputStream.close();
        System.exit(0);
    }

}
