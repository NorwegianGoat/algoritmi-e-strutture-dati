
/**
 * @author Riccardo Mioli, 825373, riccardo.mioli2@studio.unibo.it
 * @version 0.1.2b
 *
 * Viene fornito in input un grafo. E' necessario trovare il punto di uscita
 * piu' rapido per scappare a un gas. La costruzione del grafo ha costo O(n). La
 * "diffusione" dei tempi del passaggio del gas ha costo O(nm) in quanto ogni si
 * rende necessario esplorare ogni arco di ogni nodo.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.LinkedList;

public final class Esercizio5 {

    /**
     * Il link modella il concetto di un arco. Ogni link ha un tempo di
     * attraversamento degli uomini e uno per il gas. Ogni link ha anche un nodo
     * di partenza e uno di arrivo.
     */
    private static final class Arco {

        private double gasSpeed;
        private double humanSpeed;
        private Node from;
        private Node to;

        public Arco(Node from, Node to, double humanSpeed, double gasSpeed) {
            this.from = from;
            this.to = to;
            this.gasSpeed = gasSpeed;
            this.humanSpeed = humanSpeed;
        }

        public double getHumanSpeed() {
            return humanSpeed;
        }

        public double getGasSpeed() {
            return gasSpeed;
        }

        public Node getTo() {
            return to;
        }
    }

    /**
     * Ogni nodo e' dotato di una chiave che lo caratterizza e di una serie di
     * archi. Per ogni nodo e' presente anche un nodo "percorso piu' rapido" che
     * rappresenta il nodo a cui si puo' arrivare piu' rapidamente.
     */
    private static final class Node {

        private int key;
        private LinkedList<Arco> archi = new LinkedList();
        private Node mostRapidRoute = null;
        private double passaggioUmano;
        private double passaggioGas;

        public Node(int key) {
            this.key = key;
            passaggioUmano = Double.POSITIVE_INFINITY;
            passaggioGas = Double.POSITIVE_INFINITY;
        }

        public void setArchi(Node adiacente, double humanSpeed, double gasSpeed) {
            archi.add(new Arco(this, adiacente, humanSpeed, gasSpeed));
        }

        public void setPassaggioUmano(double passaggioUmano) {
            this.passaggioUmano = passaggioUmano;
        }

        public void setPassaggioGas(double passaggioGas) {
            this.passaggioGas = passaggioGas;
        }

        public void setMostRapidRoute(Node mostRapidRoute) {
            this.mostRapidRoute = mostRapidRoute;
        }

        public double getPassaggioGas() {
            return passaggioGas;
        }

        public double getPassaggioUmano() {
            return passaggioUmano;
        }

        public Node getMostRapidRoute() {
            return mostRapidRoute;
        }

        public int getKey() {
            return key;
        }

        public LinkedList<Arco> getAdiacenti() {
            return archi;
        }

        /**
         * Questo metodo consente di diffondere il gas ai nodi adiacenti.
         */
        public void spreadGas() {
            //Per ogni nodo adiacente
            for (int index = 0; index < archi.size(); index++) {
                Node adiacente = archi.get(index).getTo();
                double tempoArrivoGasANodoAdiacente = archi.get(index).getGasSpeed() + this.passaggioGas;
                System.out.print("Da " + this.getKey() + " a " + adiacente.getKey() + " il gas ci mette o " + tempoArrivoGasANodoAdiacente + " o " + adiacente.getPassaggioGas());
                //setto come tempo di passaggio del gas il minimo tra il passaggio tramite questo nodo e il tempo che e' gia' contenuto nel nodo
                adiacente.setPassaggioGas(Math.min(tempoArrivoGasANodoAdiacente, adiacente.getPassaggioGas()));
                System.out.println(" Il tempo settato e': " + adiacente.getPassaggioGas());
            }
        }

        /**
         * Questo metodo consente di diffondere il tempo di passaggio dell'uomo
         * ai nodi adiacenti
         */
        public void goHuman() {
            for (int index = 0; index < archi.size(); index++) {
                //Per ogni nodo adiacente
                Node adiacente = archi.get(index).getTo();
                double tempoArrivoUmanoANodoAdiacente = archi.get(index).getHumanSpeed() + this.passaggioUmano;
                System.out.print("Da " + this.getKey() + " a " + adiacente.getKey() + " l'umano ci mette o " + tempoArrivoUmanoANodoAdiacente + " o " + adiacente.getPassaggioUmano());
                if (tempoArrivoUmanoANodoAdiacente < adiacente.getPassaggioUmano()) {
                    /*Se il tempo di arrivo tramite questo nodo e' minore rispetto a quello che e' gia' contenuto nel nodo
                    setto come tempo migliore quello tramite questo nodo e setto come strada piu' rapida questo nodo
                    */
                    adiacente.setPassaggioUmano(tempoArrivoUmanoANodoAdiacente);
                    adiacente.setMostRapidRoute(this);
                }
                System.out.println(" Il tempo settato " + adiacente.getPassaggioUmano());
            }
        }

    }

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        //Settaggio scanner.
        Scanner inputStream = null;
        try {
            inputStream = new Scanner(new File(args[0]));
        } catch (FileNotFoundException e) {
            System.out.println("Sembra che il file di input non sia stato trovato");
            System.exit(0);
        }

        int nNodi = inputStream.nextInt();
        int nArchi = inputStream.nextInt();
        int entrata = inputStream.nextInt();
        int nodoPartenzaGas = inputStream.nextInt();
        int uscita = inputStream.nextInt();
        //Viene costruito il grafo.
        Node[] grafo = costruisciGrafo(args[0], nNodi);
        //Viene fatta partire la diffusione del gas dal nodo 0
        Node partenzaGas = grafo[nodoPartenzaGas];
        partenzaGas.setPassaggioGas(0);
        partenzaGas.spreadGas();
        //Per tutti i nodi successivi viene lanciata la procedura di diffusione del tempo del gas
        int index = nodoPartenzaGas + 1;
        int iterations = 0;
        while (iterations < nNodi) {
            if (index == nNodi) {
                index = 0;
            }
            grafo[index].spreadGas();
            index++;
            iterations++;
        }
        //Viene fatto partire l'umano dal nodo 0
        Node partenzaUomo = grafo[entrata];
        partenzaUomo.setPassaggioUmano(0);
        partenzaUomo.goHuman();
        //Per tutti i nodi successivi si fa partire la diffusione del gas
        index = entrata + 1;
        iterations = 0;
        while (iterations < nNodi) {
            if (index == nNodi) {
                index = 0;
            }
            grafo[index].goHuman();
            index++;
            iterations++;
        }
        System.out.println("Tempo uscita uomo: " + grafo[uscita].getPassaggioUmano());
        System.out.println("Tempo uscita gas: " + grafo[uscita].getPassaggioGas());
        /*Se il tempo di passaggio dell'umano nell'uscita e' piu' rapido di quello del gas
        allora sono salvo, altrimenti vuol dire che il gas ha contaminato l'uscita
        prima che io ci potessi arrivare.
         */
        if (grafo[uscita].getPassaggioUmano() < grafo[uscita].getPassaggioGas()) {
            writeOnFile(args[1], pathBuilder(grafo[uscita], grafo[entrata]));
        } else {
            writeOnFile(args[1], "-1");
        }
    }

    /**
     * Metodo per la costruzione del grafo e' richiesto il path del file di
     * input e il numero di nodi.
     *
     * @param path Il percorso dell'input file
     * @param nNodi Il numero totale di nodi
     * @return Un array contenente tutti i nodi del grafo
     */
    private static Node[] costruisciGrafo(String path, int nNodi) {
        Scanner inputStream = null;
        try {
            inputStream = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            System.out.println("Sembra che il file di input non sia stato trovato");
            System.exit(0);
        }
        inputStream.nextLine();
        Node[] grafo = new Node[nNodi];
        int index = 0;
        while (inputStream.hasNextLine() && inputStream.hasNext()) {
            int key = inputStream.nextInt();
            int keyAdiacente = inputStream.nextInt();
            double humanSpeed = inputStream.nextDouble();
            double gasSpeed = inputStream.nextDouble();
            Node adiacente = new Node(keyAdiacente);
            Node nodo = new Node(key);
            if (grafo[key] == null) {
                grafo[key] = nodo;
            }
            if (grafo[keyAdiacente] == null) {
                grafo[keyAdiacente] = adiacente;
            }
            //Al grafo del nodo adiacente viene settato il collegamento per questo nodo
            grafo[keyAdiacente].setArchi(grafo[key], humanSpeed, gasSpeed);
            //A questo nodo viene settato un arco uscente verso il nodo adiacente
            grafo[key].setArchi(grafo[keyAdiacente], humanSpeed, gasSpeed);
        }
        return grafo;
    }

    /**
     * A partire dall'uscita si scorre all'indietro il grafo seguendo i nodi con
     * i tempi minori. In questo modo si e' certi di stare tornando verso
     * l'uscita.
     *
     * @param uscita
     * @param entrata
     * @return Una stringa contenente il percorso dei nodi piu' brevi
     */
    private static String pathBuilder(Node uscita, Node entrata) {
        StringBuilder toPrint = new StringBuilder();
        do {
            toPrint.insert(0, uscita.getKey() + "\n");
            uscita = uscita.getMostRapidRoute();
        } while (uscita != entrata);
        toPrint.insert(0, uscita.getKey() + "\n");
        System.out.println("Per uscire passo tramite questi nodi: \n" + toPrint.toString());
        return toPrint.toString();
    }

    /**
     * Metodo per scrivere su file
     *
     * @param outputDestination Richiede la destinazione su cui scrivere
     * @param toWrite Richiede cosa scrivere
     */
    private static void writeOnFile(String outputDestination, String toWrite) {
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(outputDestination);
        } catch (FileNotFoundException e) {
            System.out.println("Non sono riuscito a creare il file di output nella seguente posizione: " + outputDestination);
        }
        outputStream.println(toWrite);
        outputStream.close();
        System.exit(0);
    }

}
