
/**
 * @author Riccardo Mioli, 825373, riccardo.mioli2@studio.unibo.it
 * @version 0.0.6rc
 *
 * Uno studente si trova a dover scegliere fra una serie di compiti i 16 piu'
 * profittevoli. Per risolvere il problema viene adottato un algoritmo greedy: i
 * crediti dei compiti vengono ordinati tramite quicksort dal piu' profittevole
 * al meno profittevole (i cambiamenti vengon riflessi anche sull'array delle
 * deadline). L'ordinamento ha costo O(nlogn). Una volta ordinati i compiti
 * viene usata la deadline come indice per inserire il compito nell'array dei
 * compiti scelti. Se la posizione e' gia' occupata bisogna trovare un altro
 * orario in cui fare il compito. L'operazione di scelta dei compiti piu'
 * vantaggiosi ha un costo di O(n), come anche quella di scelta di un orario
 * piu' vantaggioso.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Arrays;

public final class Esercizio3 {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        //Settaggio scanner.
        Scanner inputStream = null;
        try {
            inputStream = new Scanner(new File(args[0]));
        } catch (FileNotFoundException e) {
            System.out.println("Sembra che il file di input non sia stato trovato");
            System.exit(0);
        }

        final byte oreDisponibili = 16;
        int nAssignment = inputStream.nextInt();
        int[] deadline = new int[nAssignment];
        int[] crediti = new int[nAssignment];
        int iterations = 0;

        while (inputStream.hasNextLine() && inputStream.hasNext()) {
            crediti[iterations] = inputStream.nextInt();
            deadline[iterations] = inputStream.nextByte();
            iterations++;
        }
        //Riordinamento da piu' profittevole a meno
        quickSortModificato(crediti, deadline, 0, crediti.length - 1);
        for (int index = 0; index < deadline.length; index++) {
            System.out.println("La deadline di questo compito e': " + deadline[index] + " E mi garantisce: " + crediti[index]);
        }
        //Creo due array di appoggio uno per le deadline e uno per i crediti dei compiti selezionati
        int[] compitiSelezionati = new int[oreDisponibili];
        //I due array sono vuoti e inizializzati a 0
        int[] deadlineSelezionate = new int[oreDisponibili];
        //Per la lunghezza del vettore dei compiti da fare scorro il vettore contenente i crediti.
        for (int index = 0; index < iterations; index++) {
            /*Se la cella del compito selezionato ha un valore minore di quello 
            che mi darebbe fare quel compito
            allora scrivo il valore dei crediti che ottengo facendo
             */
            if (compitiSelezionati[deadline[index] - 1] < crediti[index]) {
                compitiSelezionati[deadline[index] - 1] = crediti[index];
                System.out.println("Ho selezionato il compito con crediti: " + compitiSelezionati[deadline[index] - 1] + "La deadline e' alle: " + deadline[index]);
                deadlineSelezionate[deadline[index] - 1] = deadline[index];
                System.out.println("L'ho messo nella cella: " + deadlineSelezionate[deadline[index] - 1]);
            } else {
                try {
                    /*Altrimenti a partire dalla cella che volevo riempire, ma 
                    che ho trovato occupata ne cerco a ritroso una che e' ancora libera.
                    Il cercare a ritroso mi garantisce di trovare un'ora prima della
                    deadline.
                     */
                    int ora = findNextFreeHour(deadlineSelezionate, deadline[index] - 1);
                    //Una volta trovata la cella libera metto i compiti da fare in quell'ora.
                    compitiSelezionati[ora] = crediti[index];
                    System.out.println("Ho selezionato il compito con crediti: " + compitiSelezionati[ora] + "La deadline e' alle: " + deadline[index]);
                    deadlineSelezionate[ora] = deadline[index];
                    System.out.println("L'ho messo nella cella: " + ora + " Anche se e' programmato per le: " + deadline[index]);
                } catch (ArrayIndexOutOfBoundsException e) {
                    /*Se entro qui significa che nessono slot  libero e' stato trovato,
                    quindi non faro' quel compito.
                     */
                }
            }
        }
        System.out.println("Crediti: " + Arrays.toString(compitiSelezionati));
        System.out.println("Deadline: " + Arrays.toString(deadlineSelezionate));
        StringBuilder toPrint = new StringBuilder();
        Integer creditiTotalizzati = 0;
        for (int index = 0; index < deadlineSelezionate.length; index++) {
            toPrint.append(compitiSelezionati[index] + " " + deadlineSelezionate[index] + "\n");
            creditiTotalizzati += compitiSelezionati[index];
        }
        toPrint.replace(0, 0, creditiTotalizzati.toString() + "\n");
        writeOnFile(args[1], toPrint.toString());
    }

    /**
     * Metodo per la ricerca di un'ora libera prima della deadline. Il vettore
     * viene scansionato totalmente, se una cella risulta essere 0 allora e'
     * possibile fare un compito in quell'ora e viene ritornato l'indice della
     * cella. Se non sono disponibili ore viene ritornato il valore sentinella
     * -1
     *
     * @param deadlineSelezionate Il vettore contenente le deadline che ho
     * scelto
     * @param start La cella da cui devo andare a ritroso
     * @return L'indice dell'ora libera
     */
    private static int findNextFreeHour(int[] deadlineSelezionate, int start) {
        //Inizializzo ad un valore sentinella negativo
        int freeHour = -1;
        //Dal valore di start vado a ritroso cercando una cella di deadline vuota
        for (start = start; start >= 0; start--) {
            //Se la trovo la riempio con il valore di start
            if (deadlineSelezionate[start] == 0) {
                freeHour = start;
                //Esco dal ciclo
                break;
            }
        }
        //Ritorno l'indice della cella vuota
        return freeHour;
    }

    /**
     * Metodo che implementa il quicksort con singolo pivot.
     *
     * @param a L'array da ordinare
     * @param b L'array su cui riflettere i cambiamenti
     * @param inizio Il punto di inizio
     * @param fine Il punto di fine
     */
    private static void quickSortModificato(int[] a, int[] b, int inizio, int fine) {
        int pivot;
        if (fine > inizio) {
            pivot = partiziona(a, b, inizio, fine);
            quickSortModificato(a, b, inizio, pivot - 1);
            quickSortModificato(a, b, pivot + 1, fine);
        }
    }

    //Metodo per spezzare l'array e ordinare le due partizioni.
    private static int partiziona(int[] a, int[] b, int inizio, int fine) {
        int x = a[fine];
        int y = b[fine];
        int i = inizio;
        int j = fine;
        int appoggioDeadline;
        int appoggioCrediti;
        while (i < j) {
            while (a[i] > x) {
                i++;
            }
            while ((i < j) && (a[j] <= x)) {
                j--;
            }
            appoggioDeadline = a[i];
            appoggioCrediti = b[i];
            if (i < j) {
                a[i] = a[j];
                a[j] = appoggioDeadline;
                b[i] = b[j];
                b[j] = appoggioCrediti;
            } else {
                a[i] = a[fine];
                a[fine] = appoggioDeadline;
                b[i] = b[fine];
                b[fine] = appoggioCrediti;
            }
        }
        return i;
    }

    /**
     * Metodo per scrivere su file
     *
     * @param outputDestination Richiede la destinazione su cui scrivere
     * @param toWrite Richiede cosa scrivere
     */
    private static void writeOnFile(String outputDestination, String toWrite) {
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(outputDestination);
        } catch (FileNotFoundException e) {
            System.out.println("Non sono riuscito a creare il file di output nella seguente posizione: " + outputDestination);
        }
        outputStream.println(toWrite);
        outputStream.close();
        System.exit(0);
    }

}
